# Auth
Athorization plugin for [Schematizer](https://www.npmjs.com/package/@schematizer/schematizer), now you can add access restrictions to queries, mutations, types and fields

To use field authorization, Auth must be passed as schematizer argument

```ts
import { schematize } from '@schematizer/schematizer'
import { AuthPlugin } from '@schematizer/auth';

const schema = await schematize(AuthPlugin(), ....);
```

## Auth for Type
You can find `auth` method at Type class

```ts
const providerType = new Type(Provider)
  .fields(...)
  .auth('SeeProviders'); // must have "SeeProviders" permission at context.permissions
```

## Auth for Fn
`auth` is also in `Fn`

```ts
interface UserInfo {
  age: number;
}

declare module '@schematizer/schematizer/dist/types' {
  interface GraphqlContext {
    user: UserInfo;
  }
}

const moviesQueries = new Queries({
  // Custom authorizer and custom message
  getMovie: Fn(...).auth(({ context }) => context.user.age >= 18 || ['only for 18+']),
});
```

## Auth for fields
The fields can be defineds as objects, this object optionaly contains `auth` prop

```ts
import { some } from '@schematizer/auth';

class UserType {
  name: string;
  address: string;
}

const userType = new Type(UserType, {
  name: {
    type: 'string',
  },
  address: {
    type: 'string',
    // Address require SeeUserAdress or Admin permissions
    auth: some(
      'SeeUserAdress',
      'Admin',
    ),
  },
});
```

## Helpers
Auth provides the following authorizers

### some
Authorize if any of the specified authorizables is met
```ts
some(...);
```

### every
Authorize if all specified authorizables are met
```ts
every(...);
```

## Authorizable
auth and authorizers requires "authorizables" to "authorize", the follow list indicates the type and behaviour

- `null | undefined | void | true`: authorization accepted
- `string`: check for permission at `context.permissions`
- `false`: fail the authorization
- `[string]`: fail the authorization set error report message
- `() => Authorizable`: trigger the function with the context and evaluate the returned value
- `{ selected: () => Authorizable }`: its the same as `() => Authorizable`
