import { Fn, GraphqlContext, Queries, schematize } from '@schematizer/schematizer';
import { graphql, GraphQLError, GraphQLSchema } from 'graphql';
import { authenticated } from '../../src';
import { serializeReport } from '../../src/report/serializeReport';

const queries = new Queries({
  test: Fn('boolean', () => () => {
    return true;
  }).auth(authenticated),
});

let schema: GraphQLSchema;
const source = `
  query {
    test
  }
`;

beforeAll(async () => {
  schema = await schematize(queries);
});

it('authenticated', async () => {
  const contextValue: GraphqlContext = {
    permissions: [],
  };
  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toBeUndefined();
  expect(data).toEqual({
    test: true,
  });
});

it('not authenticated', async () => {
  const contextValue = {};
  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport(['must be authenticated'])),
  ]);
  expect(data).toBeNull();
});

it('without context', async () => {
  const { data, errors } = await graphql({ schema, source });

  expect(errors).toEqual([
    new GraphQLError(serializeReport(['must be authenticated'])),
  ]);
  expect(data).toBeNull();
});
