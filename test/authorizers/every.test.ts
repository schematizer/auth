import { Fn, GraphqlContext, Queries, schematize } from '@schematizer/schematizer';
import { graphql, GraphQLError, GraphQLSchema } from 'graphql';
import { every } from '../../src';
import { serializeReport } from '../../src/report/serializeReport';

const queries = new Queries({
  test: Fn('boolean', () => () => {
    return true;
  }).auth(every(
    'A',
    'B',
  )),
});

let schema: GraphQLSchema;
const source = `
  query {
    test
  }
`;

beforeAll(async () => {
  schema = await schematize(queries);
});

it('authorized: with all permissions', async () => {
  const contextValue: GraphqlContext = {
    permissions: ['A', 'B'],
  };
  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toBeUndefined();
  expect(data).toEqual({
    test: true,
  });
});

it('unauthorized: without "A" permission', async () => {
  const contextValue: GraphqlContext = {
    permissions: ['B'],
  };
  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport([
      'must meet each of the following conditions',
      [
        'must have "A" permission',
      ],
    ])),
  ]);
  expect(data).toBeNull();
});

it('unauthorized: without "B" permission', async () => {
  const contextValue: GraphqlContext = {
    permissions: ['A'],
  };
  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport([
      'must meet each of the following conditions',
      [
        'must have "B" permission',
      ],
    ])),
  ]);
  expect(data).toBeNull();
});

it('unauthorized: without permissions', async () => {
  const contextValue: GraphqlContext = {
    permissions: [],
  };
  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport([
      'must meet each of the following conditions',
      [
        'must have "A" permission',
        'must have "B" permission',
      ],
    ])),
  ]);
  expect(data).toBeNull();
});

it('unauthorized: without session', async () => {
  const contextValue = {};
  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport(['must be authenticated'])),
  ]);
  expect(data).toBeNull();
});
