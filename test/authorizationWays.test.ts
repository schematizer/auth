import { Fn, GraphqlContext, Queries, schematize, Type } from '@schematizer/schematizer';
import { graphql, GraphQLError, GraphQLSchema } from 'graphql';
import { AuthPlugin } from '../src';
import { serializeReport } from '../src/report/serializeReport';

class Product {
  public id: number;
  public name: string;
  public providers: Provider[];
}

class Provider {
  public id: number;
  public name: string;
}

const productType = new Type(Product)
  .fields(() => ({
    id: 'int',
    name: 'string',
    providers: {
      type: [Provider],
      auth: () => 'providers field',
    },
  }))
  .auth('product type');

const providerType = new Type(Provider)
  .fields(() => ({
    id: 'int',
    name: 'string',
  }))
  .auth(() => 'provider type');

const queries = new Queries({
  products: Fn([Product], () => async () => {
    return [];
  }).auth({ selected: () => 'products query' } ),
  providers: Fn([Provider], () => async () => {
    return [];
  }).auth(() => 'providers query'),
});

let schema: GraphQLSchema;

const source = `
  query {
    products {
      id
      name
      providers {
        id
        name
      }
    }
    providers {
      id
      name
    }
  }
`;

beforeAll(async () => {
  schema = await schematize(productType, providerType, queries, AuthPlugin());
});

it('all permissions case', async () => {
  const contextValue: GraphqlContext = {
    permissions: [
      'products query',
      'product type',
      'providers field',
      'provider type',
      'providers query',
    ],
  };

  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toBeUndefined();
  expect(data).toEqual({
    products: [],
    providers: [],
  });
});

it('without "products query" permission', async () => {
  const contextValue: GraphqlContext = {
    permissions: [
      'product type',
      'providers field',
      'provider type',
      'providers query',
    ],
  };

  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport(['must have "products query" permission'])),
  ]);
  expect(data).toBeNull();
});

it('whitout "providers query" permission', async () => {
  const contextValue: GraphqlContext = {
    permissions: [
      'products query',
      'product type',
      'providers field',
      'provider type',
    ],
  };

  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport(['must have "providers query" permission'])),
  ]);
  expect(data).toBeNull();
});

it('whitout "product type" permission', async () => {
  const contextValue: GraphqlContext = {
    permissions: [
      'products query',
      'providers field',
      'provider type',
      'providers query',
    ],
  };

  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport(['must have "product type" permission'])),
  ]);
  expect(data).toBeNull();
});

it('whitout "provider type" permission', async () => {
  const contextValue: GraphqlContext = {
    permissions: [
      'products query',
      'product type',
      'providers field',
      'providers query',
    ],
  };

  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport(['must have "provider type" permission'])),
  ]);
  expect(data).toBeNull();
});

it('whitout "providers field" permission', async () => {
  const contextValue: GraphqlContext = {
    permissions: [
      'products query',
      'product type',
      'provider type',
      'providers query',
    ],
  };

  const { data, errors } = await graphql({ schema, source, contextValue });

  expect(errors).toEqual([
    new GraphQLError(serializeReport(['must have "providers field" permission'])),
  ]);
  expect(data).toBeNull();
});
