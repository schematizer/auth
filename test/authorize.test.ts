import { FieldSelectedInfo } from '@schematizer/schematizer';
import { authorize } from '../src';

const defaultInfo: FieldSelectedInfo = {
  context: undefined as any,
  selection: undefined as any,
};

it('true, null and undefined values', async () => {
  for (const value of [true, null, undefined]) {
    await authorize(() => value, defaultInfo);
  }
});

it ('some values by AuthorizationResult', async () => {
  for (const value of [true, null, undefined]) {
    await authorize(() => ({
      selected: () => value,
    }), defaultInfo);
  }
});

it ('some values by nested AuthorizationResult', async () => {
  for (const value of [true, null, undefined]) {
    await authorize(() => ({
      selected: () => ({
        selected: () => value,
      }),
    }), defaultInfo);
  }
});
