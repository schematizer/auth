import { FieldSelectedInfo } from '@schematizer/schematizer';
import { authorize } from '../src';

const defaultInfo: FieldSelectedInfo = {
  context: undefined as any,
  selection: undefined as any,
};

it('false value', async () => {
  const promise = authorize(() => false, defaultInfo);

  await expect(promise).rejects.toThrow('access denied');
});

it('string value', async () => {
  const promise = authorize(() => 'custom error', defaultInfo);

  await expect(promise).rejects.toThrow('custom error');
});

it('error report value', async () => {
  const promise = authorize(() => [
    'A',
    ['antelope', 'alligator', 'anaconda'],
    'B',
    ['bear', 'bird', 'beaver'],
  ], defaultInfo);

  await expect(promise).rejects.toThrow(
`A
  antelope
  alligator
  anaconda
B
  bear
  bird
  beaver`,
  );
});

it('false value by AuthorizationInfo', async () => {
  const promise = authorize(() => ({
    selected: () => false,
  }), defaultInfo);

  await expect(promise).rejects.toThrow('access denied');
});

it('string value by AuthorizationInfo', async () => {
  const promise = authorize(() => ({
    selected: () => 'custom error',
  }), defaultInfo);

  await expect(promise).rejects.toThrow('custom error');
});

it('error report value by AuthorizationInfo', async () => {
  const promise = authorize(() => ({
    selected: () => [
      'A',
      ['antelope', 'alligator', 'anaconda'],
      'B',
      ['bear', 'bird', 'beaver'],
    ],
  }), defaultInfo);

  await expect(promise).rejects.toThrow(
`A
  antelope
  alligator
  anaconda
B
  bear
  bird
  beaver`,
  );
});

it('false value by nested AuthorizationInfo', async () => {
  const promise = authorize(() => ({
    selected: () => ({
      selected: () => false,
    }),
  }), defaultInfo);

  await expect(promise).rejects.toThrow('access denied');
});
