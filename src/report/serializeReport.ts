import { ReportData } from '../types';

function serializeRow(data: ReportData, indentation = 0): string {
  const spaces = '  '.repeat(indentation);

  const result = data.map(row => {
    if (typeof row === 'string') {
      return row;
    }
    return serializeRow(row, indentation + 1);
  }).join(`\n${spaces}`);

  return `${spaces}${result}`;
}

export function serializeReport(data: ReportData): string {
  return serializeRow(data);
}
