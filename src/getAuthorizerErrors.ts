import { FieldSelectedInfo } from '@schematizer/schematizer';
import { Authorizable, ReportData } from './types';

export async function getAuthorizerErrors(
  authorizable: Authorizable,
  info: FieldSelectedInfo,
): Promise<ReportData> {
  if ( authorizable === true
    || authorizable === null
    || authorizable === undefined
    || (typeof authorizable === 'string' && authorizable.length === 0)
  ) return [];

  if (authorizable === false) {
    return ['access denied']; // generic error message
  }

  // if is a string: check permission
  if (typeof authorizable === 'string') {
    const { context } = info;
    if (!context || !context.permissions || !context.permissions.includes(authorizable)) {
      return [`must have "${authorizable}" permission`];
    }
    return [];
  }

  // if is an array: is an report error
  if (authorizable instanceof Array) {
    return authorizable;
  }

  const authorizer = typeof authorizable === 'function' ? authorizable : authorizable.selected;
  const result = await getAuthorizerErrors(
    await authorizer(info),
    info,
  );

  return result;
}
