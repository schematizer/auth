import { Arrayable, FieldSelectedInfo } from '@schematizer/schematizer';

// export type ValueAuthorizer = () => AuthorizationResult
//   | AuthorizationInfo
// ;

export type Authorizer = (context: FieldSelectedInfo) =>
  AuthorizationResult
  | AuthorizationInfo
  // | ValueAuthorizer; // TODO
  | Promise <
    AuthorizationResult
    | AuthorizationInfo
    // | ValueAuthorizer; // TODO
  >
;

export interface AuthorizationInfo {
  selected: Authorizer;
  // resolved: ValueAuthorizer; // TODO
}

export type ReportData = Array<string | ReportData>;

export type AuthorizationResult = null | undefined | void | boolean | string | ReportData;

export type Authorizable = Authorizer | AuthorizationResult | AuthorizationInfo;

declare module '@schematizer/schematizer/dist/types' {
  interface GraphqlContext {
    permissions: string[];
  }

  interface TypeField<
    Target = any,
    Field extends Arrayable<VarType<any>> = any,
  > extends VarInfo<Field> {
    auth?: Authorizable;
  }
}
