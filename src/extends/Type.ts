import { Type } from '@schematizer/schematizer';
import { authorize } from '../authorize';
import { Authorizable, Authorizer } from '../types';

declare module '@schematizer/schematizer/dist/definers' {
  interface Type<Target extends object = any> {
    auth: (authorizer: Authorizable) => Type<Target>;
  }
}

Type.prototype.auth = function auth(authorizable: Authorizable) {
  this.onSelect(async info => {
    await authorize(authorizable, info);
  });
  return this;
};
