import { FunctionDeclaration, VarType } from '@schematizer/schematizer';
import { authorize } from '../authorize';
import { Authorizable } from '../types';

declare module '@schematizer/schematizer/dist/definers' {
  interface FunctionDeclaration<Returns extends VarType = any> {
    auth: (authorizer: Authorizable) => FunctionDeclaration<Returns>;
  }
}

FunctionDeclaration.prototype.auth = function auth(authorisable: Authorizable) {
  this.onSelect(async ({ context, selection: { selection } }) => {
    await authorize(authorisable, {
      context,
      selection,
    });
  });
  return this;
};
