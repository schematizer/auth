import { FieldSelectedInfo } from '@schematizer/schematizer';
import { getAuthorizerErrors } from './getAuthorizerErrors';
import { serializeReport } from './report/serializeReport';
import { Authorizable } from './types';

export async function authorize<Type>(authorizable: Authorizable, info: FieldSelectedInfo) {
  const errors = await getAuthorizerErrors(authorizable, info);
  if (errors.length) {
    throw new Error(serializeReport(errors));
  }
}
