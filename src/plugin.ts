import { SchematizerPlugin, StrictTypeField } from '@schematizer/schematizer';
import { TypeSchematizer } from '@schematizer/schematizer/dist/schematizers';
import { authorize } from './authorize';

function bindFieldEvents(field: StrictTypeField<any>) {
  field.onSelect.push(async info => {
    await authorize(field.auth, info);
  });
}

function prepareTypeFields(type: TypeSchematizer) {
  Object
    .values(type.fields)
    .filter(field => field.auth)
    .forEach(bindFieldEvents);
}

export function AuthPlugin() {
  const plugin: SchematizerPlugin = ({ store, onSchematize }) => {
    onSchematize(() => {
      store.types.forEach(prepareTypeFields);
    });
  };

  return plugin;
}
