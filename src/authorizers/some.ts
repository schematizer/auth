import { Authorizer } from '../types';
import { getErrors } from './getErrors';

export function some(...validators: Array<string | Authorizer>) {
  const authorizer: Authorizer = async info => {
    const result = await getErrors(info, ...validators);

    if (result.sessionErrors) {
      return result.sessionErrors;
    }

    const hasFailed = validators.length === result.errors.length;
    if (!hasFailed) return true;

    return [
      'must meet any of the following conditions',
      result.errors,
    ];
  };

  return authorizer;
}
