import { FieldSelectedInfo } from '@schematizer/schematizer';
import { getAuthorizerErrors } from '../getAuthorizerErrors';
import { Authorizer, ReportData } from '../types';
import { authenticated } from './authenticated';

export type getErrorsResult =
  { sessionErrors: ReportData } |
  { errors: ReportData }
;

export async function getErrors(
  info: FieldSelectedInfo,
  ...validators: Array<string | Authorizer>
) {
  const sessionErrors = await getAuthorizerErrors(authenticated, info);
  if (sessionErrors.length) {
    return { sessionErrors };
  }

  const errors: ReportData = [];
  for (const validator of validators) {
    const newErrors = await getAuthorizerErrors(validator, info);
    if (newErrors.length) {
      errors.push(...newErrors);
    }
  }

  return { errors };
}
