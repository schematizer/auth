import { Authorizer } from '../types';

export const authenticated: Authorizer = ({ context }) => {
  if (!context || !context.permissions) {
    return ['must be authenticated'];
  }

  return true;
};
