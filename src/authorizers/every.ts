import { Authorizer } from '../types';
import { getErrors } from './getErrors';

export function every(...validators: Array<string | Authorizer>) {
  const authorizer: Authorizer = async info => {
    const result = await getErrors(info, ...validators);

    if (result.sessionErrors) {
      return result.sessionErrors;
    }

    const hasFailed = result.errors.length > 0;
    if (!hasFailed) return true;

    return [
      'must meet each of the following conditions',
      result.errors,
    ];
  };

  return authorizer;
}
